<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 05.05.16
 * Time: 11:22
 */

namespace CMS\LocalizationBundle\Helper;


class LocaleHelper
{
    static function normalizeData($data, $locale, $defaultLocale = null){
        $locale = strtolower((string) $locale);

        if (is_array($data)){
            $result = null;
            foreach($data as $code => $value){
                if(strtoupper($code) == strtoupper($locale)){
                    $result = $value;
                    break;
                }
            }

            if($defaultLocale) {
                if (empty($result) && (isset($data[$defaultLocale]) && !empty($data[$defaultLocale]))) {
                    $result = $data[$defaultLocale];
                }
            }

            if(empty($result)){
                $result = reset($data);
            }

            return $result;
        }

        return $data;
    }
}