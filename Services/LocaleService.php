<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 20.01.15
 * Time: 9:02
 */

namespace CMS\LocalizationBundle\Services;

use CMS\CoreBundle\AbstractCoreService;
use CMS\LocalizationBundle\Form\LocaleType;
use CMS\LocalizationBundle\Helper\LocaleHelper;
use Symfony\Component\Form\FormFactory;

class LocaleService extends AbstractCoreService
{
    public function normalizeData($data){
        return LocaleHelper::normalizeData(
            $data,
            $this->container->getParameter('locale'),
            $this->container->getParameter('kernel.default_locale')
        );
    }

    public function getLocales()
    {
        return $this->getRepository()->getLocales();
    }

    /**
     * @return string
     */
    public function getCurrentLocale()
    {
        $requestStack = $this->container->get('request_stack');

        $request = $requestStack->getCurrentRequest();

        return $request->getLocale();
    }

    /**
     * @return array
     */
    public function getDefaultsCriteria()
    {
        return array();
    }

    public function configureForm(FormFactory $form, $data = null)
    {
        return $form->createBuilder(
            LocaleType::class,
            $data,
            array(
                'data_class' => $this->getRepositoryClass()
            )
        );
    }

    /**
     * Return name repository for crud
     *
     * @return string
     */
    public function getRepositoryName()
    {
        return 'LocalizationBundle:Locale';
    }
}