<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 28.06.15
 * Time: 20:14
 */

namespace CMS\LocalizationBundle\Services;


use CMS\CoreBundle\AbstractCoreService;
use CMS\LocalizationBundle\Form\CountriesType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormFactory;

class CountriesService extends AbstractCoreService
{
    /**
     * @return array
     */
    public function getDefaultsCriteria()
    {
        return array();
    }

    /**
     * Return form for entity
     *
     * @param FormBuilder|FormFactory $form
     * @param null $data
     * @return mixed
     */
    public function configureForm(FormFactory $form, $data = null)
    {
        return $form->createBuilder(
            new CountriesType(),
            $data,
            array(
                'data_class' => $this->getRepositoryClass()
            )
        );
    }

    /**
     * Return name repository for crud
     *
     * @return string
     */
    public function getRepositoryName()
    {
        return 'LocalizationBundle:Countries';
    }
}