<?php
/**
 * Created by PhpStorm.
 * User: Pavel
 * Date: 15.02.15
 * Time: 18:10
 */

namespace CMS\LocalizationBundle\Services;


use CMS\CoreBundle\AbstractCoreService;
use CMS\LocalizationBundle\Form\CitiesType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormFactory;

class CitiesService extends AbstractCoreService
{
    public function getByAlias($alias)
    {
        $repository = $this->getRepository();

        $builder = $repository->createQueryBuilder('c');

        $query = $builder->select(array('c', 'cc'))
            ->leftJoin('c.country', 'cc')
            ->andWhere('c.alias = :alias')
            ->setParameter('alias', $alias)
            ->getQuery()
            ->useQueryCache(true)
            ->useResultCache(true)
            ->setResultCacheLifetime(86400)
            ->setQueryCacheLifetime(86400);

        return $query->getOneOrNullResult();
    }

    /**
     * @return array
     */
    public function getDefaultsCriteria()
    {
        return array();
    }

    /**
     * Return name repository for crud
     *
     * @return string
     */
    public function getRepositoryName()
    {
        return 'LocalizationBundle:Cities';
    }

    /**
     * Return form for entity
     *
     * @param FormBuilder|FormFactory $form
     * @param null $data
     * @return mixed
     */
    public function configureForm(FormFactory $form, $data = null)
    {
        return $form->createBuilder(
            new CitiesType(),
            $data,
            array(
                'data_class' => $this->getRepositoryClass()
            )
        );
    }
}