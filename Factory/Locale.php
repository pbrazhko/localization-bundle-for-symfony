<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 23.01.15
 * Time: 21:58
 */

namespace CMS\LocalizationBundle\Factory;


class Locale extends \ArrayIterator implements \ArrayAccess
{
    private $value;

    public function __construct($value)
    {
        if (null === $value || empty($value)) {
            $this->value = array();
        } else {
            $data = @unserialize($value);

            if (false === $data) {
                return array($value);
            }

            $this->value = $data;
        }

        parent::__construct($this->value);
    }

    public function __call($method, $arguments)
    {
        $typeMethod = substr($method, 0, 3);
        $locale = substr($method, 3);

        if ($typeMethod === 'get') {
            if (isset($this->value[$locale])) {
                return $this->value[$locale];
            }

            return null;
        } elseif ($typeMethod === 'set') {
            $this->value[$locale] = $arguments;
        } else {
            throw new \BadMethodCallException(sprintf('Method \'%s\' not exists!', $method));
        }
    }

    public function __get($locale)
    {
        if (isset($this->value[$locale])) {
            return $this->value[$locale];
        }

        return null;
    }

    public function __set($locale, $value)
    {
        $this->value[$locale] = $value;

        return $this;
    }

    public function __toString()
    {
        return serialize($this->value);
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Whether a offset exists
     * @link http://php.net/manual/en/arrayaccess.offsetexists.php
     * @param mixed $offset <p>
     * An offset to check for.
     * </p>
     * @return boolean true on success or false on failure.
     * </p>
     * <p>
     * The return value will be casted to boolean if non-boolean was returned.
     */
    public function offsetExists($offset)
    {
        return true;
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Offset to retrieve
     * @link http://php.net/manual/en/arrayaccess.offsetget.php
     * @param mixed $offset <p>
     * The offset to retrieve.
     * </p>
     * @return mixed Can return all value types.
     */
    public function offsetGet($offset)
    {
        if (isset($this->value[$offset])) {
            return $this->value[$offset];
        }

        return null;
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Offset to set
     * @link http://php.net/manual/en/arrayaccess.offsetset.php
     * @param mixed $offset <p>
     * The offset to assign the value to.
     * </p>
     * @param mixed $value <p>
     * The value to set.
     * </p>
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        $this->value[$offset] = $value;

        return $this;
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Offset to unset
     * @link http://php.net/manual/en/arrayaccess.offsetunset.php
     * @param mixed $offset <p>
     * The offset to unset.
     * </p>
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->value[$offset]);

        return $this;
    }
}