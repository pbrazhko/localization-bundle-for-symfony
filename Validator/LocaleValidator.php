<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 19.11.15
 * Time: 14:04
 */

namespace CMS\LocalizationBundle\Validator;

use CMS\LocalizationBundle\Services\LocaleService;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class LocaleValidator extends ConstraintValidator
{
    const VALIDATION_MODE_CURRENT = 'current';
    const VALIDATION_MODE_FULL = 'full';

    /**
     * @var LocaleService
     */
    private $localeService = null;

    private $locales = null;

    /**
     * LocaleValidator constructor.
     * @param LocaleService $localeService
     */
    public function __construct(LocaleService $localeService)
    {
        $this->localeService = $localeService;
    }

    /**
     * @return null|array
     */
    private function getLocales()
    {
        if (null === $this->locales) {
            $this->locales = $this->localeService->getLocales();
        }

        return $this->locales;
    }

    /**
     * Checks if the passed value is valid.
     *
     * @param mixed $value The value that should be validated
     * @param Constraint $constraint The constraint for the validation
     */
    public function validate($value, Constraint $constraint)
    {
        if (!is_array($value)) {
            $this->context->buildViolation('validation.locale_string.type')
                ->addViolation();
        }

        switch ($constraint->mode) {
            case self::VALIDATION_MODE_CURRENT:
                $this->validateCurrentMode($value);
                break;
            case self::VALIDATION_MODE_FULL:
                $this->validateFullMode($value);
                break;
            default:
                $this->context->buildViolation('validation.locale_string.unresolved_validation_mode')
                    ->addViolation();
        }
    }

    private function validateFullMode($value)
    {
        foreach ($this->getLocales() as $locale) {
            if (!isset($value[$locale->getCode()]) || empty($value[$locale->getCode()])) {
                $this->context->buildViolation('validation.locale_string.' . $locale->getCode() . '.not_blank')
                    ->setParameter('%string%', isset($value[$locale->getCode()]) ?: null)
                    ->addViolation();

                break;
            }
        }
    }

    private function validateCurrentMode($value)
    {
        $currentLocale = $this->localeService->getCurrentLocale();

        if (!isset($value[$currentLocale]) || empty($value[$currentLocale])) {
            $this->context->buildViolation('validation.locale_string.' . $currentLocale . '.not_blank')
                ->setParameter('%string%', $value[$currentLocale])
                ->addViolation();
        }
    }
}