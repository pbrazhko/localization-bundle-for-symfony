/**
 * Created by pavel on 10.02.15.
 */

$(document).ready(function () {
    var tabs = $('.locale-form-tabs').find('li');

    if (tabs.length > 0) {
        tabs.each(function (index, element) {
            $(element).click(function (e) {
                hideFormFields($(this).find('a').attr('data-locale-change'));
                setActiveFormTab(element);
                e.preventDefault();
            });
        });
    }

    function setActiveFormTab(activeElement) {
        if (activeElement == undefined) return;

        if (tabs.length > 0) {
            tabs.each(function (index, element) {
                if ($(element).hasClass('active')) {
                    $(element).removeClass('active');
                }
            });
        }

        $(activeElement).addClass('active');
    }

    function hideFormFields(currentLocale) {
        if (currentLocale == undefined) return;

        $('input[data-locale]').each(function (index, element) {
            var locale = $(element).attr('data-locale');
            if (locale !== currentLocale) {
                $(element).hide();
            }
            else {
                $(element).show();
            }
        });

        $('textarea[data-locale]').each(function (index, element) {
            var locale = $(element).attr('data-locale');
            if (locale !== currentLocale) {
                $(element).hide();
            }
            else {
                $(element).show();
            }
        });
    }
});
