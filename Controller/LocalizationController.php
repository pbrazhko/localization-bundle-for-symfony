<?php

namespace CMS\LocalizationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class LocalizationController
 * @package CMS\LocalizationBundle\Controller
 */
class LocalizationController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction()
    {
        $service = $this->get('cms.localization.service');

        return $this->render('LocalizationBundle:Locale:list.html.twig', array(
            'locales' => $service->findAll()
        ));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function createAction(Request $request)
    {
        $service = $this->get('cms.localization.service');

        $form = $service->generateForm();

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $data = $form->getData();

                $service->create($data);

                return $this->redirect($this->generateUrl('cms_localization_list'));
            }
        }

        return $this->render('LocalizationBundle:Locale:edit.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function editAction(Request $request, $id)
    {
        $service = $this->get('cms.localization.service');

        $form = $service->generateForm($service->findOneById($id));

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $data = $form->getData();

                $service->update($data);

                return $this->redirect($this->generateUrl('cms_localization_list'));
            }
        }

        return $this->render('LocalizationBundle:Locale:edit.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction($id)
    {
        $service = $this->get('cms.localization.service');

        $service->delete($id);

        return $this->redirect($this->generateUrl('cms_localization_list'));
    }
}
