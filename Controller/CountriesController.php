<?php

namespace CMS\LocalizationBundle\Controller;

use CMS\LocalizationBundle\Entity\Countries;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class CountriesController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $service = $this->get('cms.localization.countries.service');

        return $this->render('LocalizationBundle:Countries:list.html.twig', array(
            'countries' => $service->findAll()
        ));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function createAction(Request $request)
    {
        $service = $this->get('cms.localization.countries.service');

        $form = $service->generateForm();

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $service->create($form->getData());

                return $this->redirect($this->generateUrl('cms_localization_countries_list'));
            }
        }

        return $this->render('LocalizationBundle:Countries:edit.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function editAction(Request $request, $id)
    {
        $service = $this->get('cms.localization.countries.service');

        $form = $service->generateForm($service->findOneById($id));

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $service->update($form->getData());

                return $this->redirect($this->generateUrl('cms_localization_countries_list'));
            }
        }

        return $this->render('LocalizationBundle:Countries:edit.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, $id)
    {
        $service = $this->get('cms.localization.countries.service');

        $service->delete($id);

        return $this->redirect($this->generateUrl('cms_localization_countries_list'));
    }
}
