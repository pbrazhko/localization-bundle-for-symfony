<?php

namespace CMS\LocalizationBundle\Controller;

use CMS\LocalizationBundle\Entity\Cities;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class CitiesController
 * @package CMS\HotelsBundle\Controller
 */
class CitiesController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $service = $this->get('cms.localization.cities.service');

        return $this->render('LocalizationBundle:Cities:list.html.twig', array(
            'cities' => $service->findAll()
        ));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function createAction(Request $request)
    {
        $service = $this->get('cms.localization.cities.service');

        $form = $service->generateForm();

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $service->create($form->getData());

                return $this->redirect($this->generateUrl('cms_localization_cities_list'));
            }
        }

        return $this->render('LocalizationBundle:Cities:edit.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function editAction(Request $request, $id)
    {
        $service = $this->get('cms.localization.cities.service');

        $form = $service->generateForm($service->findOneById($id));

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $service->update($form->getData());

                return $this->redirect($this->generateUrl('cms_localization_cities_list'));
            }
        }

        return $this->render('LocalizationBundle:Cities:edit.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, $id)
    {
        $service = $this->get('cms.localization.cities.service');

        $service->delete($id);

        return $this->redirect($this->generateUrl('cms_localization_cities_list'));
    }
}
