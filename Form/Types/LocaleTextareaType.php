<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 21.01.15
 * Time: 9:06
 */

namespace CMS\LocalizationBundle\Form\Types;

use CMS\LocalizationBundle\Services\LocaleService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;

class LocaleTextareaType extends AbstractType
{
    private $locales = array();
    private $currentLocale;

    public function __construct(LocaleService $service)
    {
        $localeRepository = $service->getRepository();
        $query = $localeRepository->createQueryBuilder('l')
            ->select(array('l'))
            ->getQuery()
            ->useQueryCache(true)
            ->setQueryCacheLifetime(86400)
            ->setResultCacheLifetime(86400)
            ->useResultCache(true);

        $this->locales = $query->getResult();
        $this->currentLocale = $service->getCurrentLocale();
    }

    public function buildForm(FormBuilderInterface $builder, array $options = array())
    {
        foreach ($this->locales as $locale) {
            $_options = array_merge_recursive($builder->getOptions(), array(
                'attr' => array(
                    'data-locale' => $locale->getCode()
                )
            ));

            $_options['compound'] = false;

            $builder->add(strtolower($locale->getCode()), TextareaType::class, $_options);
        }

        return $builder;
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'cms_localization_textarea_type';
    }
}