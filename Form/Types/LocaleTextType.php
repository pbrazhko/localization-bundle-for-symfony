<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 21.01.15
 * Time: 9:06
 */

namespace CMS\LocalizationBundle\Form\Types;

use CMS\LocalizationBundle\Services\LocaleService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class LocaleTextType extends AbstractType
{
    private $locales = array();
    private $currentLocale;

    public function __construct(LocaleService $service)
    {
        $localeRepository = $service->getRepository();
        $query = $localeRepository->createQueryBuilder('l')
            ->select(array('l'))
            ->getQuery()
            ->useQueryCache(true)
            ->setQueryCacheLifetime(86400)
            ->setResultCacheLifetime(86400)
            ->useResultCache(true);

        $this->locales = $query->getResult();
        $this->currentLocale = $service->getCurrentLocale();
    }

    public function buildForm(FormBuilderInterface $builder, array $options = array())
    {
        foreach ($this->locales as $locale) {
            $childOptions = [];
            $childOptions['attr'] = array_merge($options['attr'], ['data-locale' => $locale->getCode()]);

            $builder->add(strtolower($locale->getCode()), TextType::class, $childOptions);
        }

        return $builder;
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'cms_localization_text_type';
    }
}