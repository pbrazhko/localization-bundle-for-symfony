<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 21.01.15
 * Time: 9:06
 */

namespace CMS\LocalizationBundle\Form\Types;

use CMS\LocalizationBundle\Services\LocaleService;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LocaleEntityType extends AbstractType
{
    private $localeService;
    private $locales = array();
    private $currentLocale;

    public function __construct(LocaleService $service)
    {
        $this->localeService = $service;

        $localeRepository = $service->getRepository();
        $query = $localeRepository->createQueryBuilder('l')
            ->select(array('l'))
            ->getQuery()
            ->useQueryCache(true)
            ->setQueryCacheLifetime(86400)
            ->setResultCacheLifetime(86400)
            ->useResultCache(true);

        $this->locales = $query->getResult();
        $this->currentLocale = $service->getCurrentLocale();
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setNormalizer('choice_label', function (OptionsResolver $resolver, $property) {
            return $property . '[' . $this->currentLocale . ']';

        });

        $resolver->setNormalizer('group_by', function (OptionsResolver $resolver, $property) {
            return $property ? $property . '[' . $this->currentLocale . ']' : $property;
        });

        $resolver->setDefaults(array(
            'current_locale' => $this->currentLocale,
            'choice_label' => 'title'
        ));
    }

    public function getParent()
    {
        return EntityType::class;
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'cms_localization_entity_type';
    }
}