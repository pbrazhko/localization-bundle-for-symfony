<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 02.02.15
 * Time: 10:27
 */

namespace CMS\LocalizationBundle\Listeners;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Router;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class LocaleListener implements EventSubscriberInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var Router
     */
    private $router;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->router = $this->container->get('router');
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();

        if (!$request->hasPreviousSession()) {
            return;
        }

        if (null !== ($locale = $request->query->get('_locale'))) {
            $request->getSession()->set('_locale', $locale);
            $request->setLocale($locale);

            $routeParams = $request->get('_route_params');
            $routeParams['_locale'] = $locale;

            $url = $this->router->generate($request->get('_route'), $routeParams);

            $response = new RedirectResponse($url);
            $event->setResponse($response);
        } else {
            $defaultLocale = $this->container->getParameter('kernel.default_locale');

            $request->setLocale($request->getSession()->get('_locale', $defaultLocale));
        }
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * array('eventName' => 'methodName')
     *  * array('eventName' => array('methodName', $priority))
     *  * array('eventName' => array(array('methodName1', $priority), array('methodName2'))
     *
     * @return array The event names to listen to
     *
     * @api
     */
    public static function getSubscribedEvents()
    {
        return array(
            KernelEvents::REQUEST => array(array('onKernelRequest', 17))
        );
    }

}