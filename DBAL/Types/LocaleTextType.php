<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 21.01.15
 * Time: 15:31
 */

namespace CMS\LocalizationBundle\DBAL\Types;

use CMS\LocalizationBundle\Factory\Locale;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\ConversionException;
use Doctrine\DBAL\Types\Type;

class LocaleTextType extends Type
{

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return null === $value ? null : serialize(array_change_key_case($value, CASE_LOWER));
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if (empty($value)) {
            return null;
        }

        $value = (is_resource($value)) ? stream_get_contents($value) : $value;
        $val = unserialize($value);
        if ($val === false && $value != 'b:0;') {
            throw ConversionException::conversionFailed($value, $this->getName());
        }

        return array_change_key_case($val, CASE_LOWER);
    }

    public function requiresSQLCommentHint(AbstractPlatform $platform)
    {
        return true;
    }

    /**
     * Gets the SQL declaration snippet for a field of this type.
     *
     * @param array $fieldDeclaration The field declaration.
     * @param \Doctrine\DBAL\Platforms\AbstractPlatform $platform The currently used database platform.
     *
     * @return string
     */
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return 'text';
    }

    /**
     * Gets the name of this type.
     *
     * @return string
     *
     * @todo Needed?
     */
    public function getName()
    {
        return 'locale_text';
    }
}