<?php

namespace CMS\LocalizationBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class LocalizationBundle extends Bundle
{
    public function isEnabled()
    {
        return true;
    }

    public function getDescription()
    {
        return array(
            array(
                'title' => 'Languages',
                'defaultRoute' => 'cms_localization_list'
            ),
            array(
                'title' => 'Cities',
                'defaultRoute' => 'cms_localization_cities_list'
            ),
            array(
                'title' => 'Countries',
                'defaultRoute' => 'cms_localization_countries_list'
            )
        );
    }
}
