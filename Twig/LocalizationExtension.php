<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 02.02.15
 * Time: 9:52
 */

namespace CMS\LocalizationBundle\Twig;


use CMS\LocalizationBundle\Entity\Locale;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Twig_Environment;

class LocalizationExtension extends \Twig_Extension
{
    /**
     * @var ContainerInterface
     */
    private $container;
    
    /**
     * @var ArrayCollection
     */
    private $locales;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('show_locales', [$this, 'showLocalesFunction'], ['is_safe' => ['html'], 'needs_environment' => true]),
            new \Twig_SimpleFunction('show_locale_tabs', [$this, 'showLocaleTabsFunction'], ['is_safe' => ['html'], 'needs_environment' => true])
        );
    }

    public function getFilters(){
        return array(
            new \Twig_SimpleFilter('locale', [$this, 'localeFilter'], ['is_safe' => ['html'], 'needs_environment' => true])
        );
    }

    public function showLocalesFunction(Twig_Environment $environment, $template = 'LocalizationBundle:Twig:locales.html.twig')
    {
        $requestStack = $this->container->get('request_stack');
        $request = $requestStack->getCurrentRequest();

        $route = $request->get('_route');

        $locales = null;
        $_locales = $this->getLocales();

        if ($_locales) {
            /** @var Locale $locale */
            foreach ($_locales as $locale) {
                $locales[$locale->getCode()] = $locale;
            }
        }

        return $environment->render(
            $template,
            array(
                'route' => $route,
                'locales' => $locales
            )
        );
    }

    public function showLocaleTabsFunction(\Twig_Environment $environment, $template = 'LocalizationBundle:Twig:locale_tabs.html.twig')
    {
        return $environment->render(
            $template,
            array(
                'locales' => $this->getLocales()
            )
        );
    }

    public function localeFilter(Twig_Environment $environment, $data){
        $requestStack = $this->container->get('request_stack');
        $request = $requestStack->getCurrentRequest();

        $defaultLocale = strtolower($this->container->getParameter('kernel.default_locale'));

        $locale = strtolower($request->getLocale());
        if (is_array($data)){
            $result = null;
            foreach($data as $code => $value){
                if(strtoupper($code) == strtoupper($locale)){
                    $result = $value;
                    break;
                }
            }

            if(empty($result) && (isset($data[$defaultLocale]) && !empty($data[$defaultLocale]))){
                $result = $data[$defaultLocale];
            }

            if(empty($result)){
                $result = reset($data);
            }

            return $result;
        }

        return $data;
    }

    private function getLocales()
    {
        if (null === $this->locales) {
            $service = $this->container->get('cms.localization.service');
            $localeRepository = $service->getRepository();
            $query = $localeRepository->createQueryBuilder('l')
                ->select(array('l'))
                ->getQuery()
                ->useQueryCache(true)
                ->setQueryCacheLifetime(86400)
                ->setResultCacheLifetime(86400)
                ->useResultCache(true);


            $this->locales = $query->getResult();
        }

        return $this->locales;
    }

    public function getName()
    {
        return 'cms_localization_extension';
    }
}